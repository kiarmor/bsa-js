export function fight(firstFighter, secondFighter) {
    const fighterA = firstFighter;
    const fighterB = secondFighter;
    let winner;

    while (fighterA.health > 0 && fighterB.health > 0){
        fighterA.health = fighterA.health - getDamage(fighterB, fighterA);
        if (fighterA.health <= 0){
            return winner = fighterB;
        }
        fighterB.health = fighterB.health - getDamage(fighterA, fighterB);
        if (fighterB.health <= 0){
            return winner = fighterA;
        }
    }
}

    export function getDamage(attacker, enemy) {
    const hit = getHitPower(attacker) ;
    const block = getBlockPower(enemy);
    const damage = hit - block;
    if (damage < 0){
        return 0;
    }

    return damage;
}

    export function getHitPower(fighter) {
    const  criticalHitChance = Math.random() + 1;
    const  attack = fighter.attack;
    const hit_power = criticalHitChance * attack;

    return hit_power;
}

    export function getBlockPower(fighter) {
    const  dodgeChance = Math.random() + 1;
    const  defense = fighter.defense;
    const  block_power = dodgeChance * defense;

    return block_power;
}